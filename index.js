'use strict';

const fs      = require('fs');
const glob    = require('glob');
const path    = require('path');
const resolve = require('resolve');

// Pre-defined glob patterns
const options = {
  regex  : new RegExp('.(sc|sa|c)ss'),
  exts   : '.@(sc|sa|c)ss',
  prefix : '?(_)',
  extPreferenceOrder: ['.scss', '.sass', '.css'] // lower index = higher preference
};

// const importedFiles = {};

const getSassGlob = function(filePath) {
  const parsedPath = path.parse(filePath);

  // Prefer to use the combination of name and extension, rather than base (they're identical)
  delete parsedPath.base;

  // ===========================================================================
  // DIRECTORY CHECK
  // If the parsed path extension doesn't exist or doesn't match, then check it as a directory path
  // ===========================================================================
  if (parsedPath.ext === '' || !options.regex.test(parsedPath.ext)) {
    // Try to check the filePath as a directory and ensure any errors are caught
    try {
      // If it's a directory, then pass the file path as a wildcard directory path
      if (fs.statSync(filePath).isDirectory()) {
        return getSassGlob(`${filePath}/*`);
      }
    } catch(error) {
      // do nothing and assume the filePath is just an accurate extension-less file
    }
  }

  // ===========================================================================
  // EXTENSION CHECK
  // Add the extension glob if the extension doesn't already exist
  // ===========================================================================
  if (parsedPath.ext === '') {
    parsedPath.ext = options.exts;
  } else if (!options.regex.test(parsedPath.ext)) {
    // Append the extension glob to the already existing extension
    parsedPath.ext += options.exts;
  }

  // ===========================================================================
  // PREFIX CHECK
  // Add the underscore prefix glob if it doesn't already exist
  // ===========================================================================
  if (!parsedPath.name.includes('_')) {
    parsedPath.name = `${options.prefix}${parsedPath.name}`;
  }

  // Re-format the parsed path object back into a full path
  return path.format(parsedPath);
};

// Filter out file paths with identical names, but different extensions by using extPreferenceOrder
const getFilteredPaths = function(filePaths) {
  // Object store for "filtered" urls
  const filteredPathCache = {};

  // Loop over URLs and store ones of higher preference
  filePaths.forEach((filePath) => {
    const pathExtension = path.extname(filePath);
    const extensionless = filePath.slice(0, -pathExtension.length); // full file path without the extension

    // Store the file path if it hasn't been stored yet
    if (filteredPathCache[extensionless] == null) {
      filteredPathCache[extensionless] = pathExtension;
    } else {
      const extension = filteredPathCache[extensionless];
      const extPreferenceIndex = Math.min(options.extPreferenceOrder.indexOf(extension), options.extPreferenceOrder.indexOf(pathExtension));

      // Overwrite the stored extension with the one of higher preference
      filteredPathCache[extensionless] = options.extPreferenceOrder[extPreferenceIndex];
    }
  });

  // Map over the stored data and return a concatenation of the key and value to form the full file path
  return Object.keys(filteredPathCache).map(key => `${key}${filteredPathCache[key]}`);
};

const getDoneObject = function (files) {
  // If there's only 1 file, then return that path string for use with node-sass's done
  if (files.length === 1) {
    const file = files[0];

    // // Skip over already imported files
    // if (importedFiles[file]) { return { contents: '' }; }

    // // Flag the file to prevent duplicate imports
    // importedFiles[file] = true;

    return { file };
  }

  const contents = files.map(function (fileName) {
    return `@import ${JSON.stringify(fileName)};`;
  }).join('\n');

  return { contents: contents };
};

// Try to resolve the path as a node module. Format return for the node-sass done callback.
const resolvePath = function(url, basedir) {
  try {
    return {
      file: resolve.sync(url, {
        basedir,
        extensions: options.extPreferenceOrder
      })
    }
  } catch(error) {
    return error;
  }
}

module.exports = function(url, file, done) {
  // Resolve the file name based on the relative directory (file dirname) and file name (url)
  const fullPath = path.resolve(path.dirname(file), url);

  // Parse the full path into a glob-friendly path name (or array of paths)
  const globPath = getSassGlob(fullPath);

  // Parse the glob into real paths
  glob(globPath, function(error, urls) {
    if (error) { throw error; }

    if (urls.length === 0) {
      return done(resolvePath(url, file));
    }

    // Filter the paths by removing identical files with different extensions
    const paths = getFilteredPaths(urls);

    done(getDoneObject(paths));
  });
}
