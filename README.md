# node-sass-dedupe-import


# TO-DO

- add test cases
- potentially leverage `resolve` package instead of hand-written code



# Tests

Directory structure   
*contrived example:*  
```
assets
  css
    components
      _component.scss
      component.css
      _test.component.scss
    main.scss
  _test.scss
```

In `main.scss`:

These should all pass. Goal is to keep the author's original intent, thus explicitly written
file extensions are preserved.

Statement                                   Testing for?
---------                                   ------------
`@import '../test'`                         - directory traverse up with `../`
`@import '../_test'`                        - explicit file name with `_`
`@import '../_test.scss'`                   - explicit file name with `_` and extension



`@import 'components/component'`            - regular nested import, that pulls SCSS version instead of css version
`@import 'components/component.scss'`       - explicit file extension ( scss )
`@import 'components/component.sass'`       - explicit file extension ( sass )
`@import 'components/component.css'`        - explicit file extension ( css )

`@import 'components/test.component'`       - multi-dot file name without true extension
`@import 'components/test.component.scss'`  - multi-dot file name WITH explicit extension
`@import 'components/*'`                    - wild-card full directory and all contents





Tests with `gulp styles:watch` in `gulp-build` repo:    

`node-sass-import` - WORKS (wrong final css order)
`node-sass-dedupe-import` - FAILS
`node-sass-import` clone with `path.format` - WORKS (wrong final css order)
`node-sass-import` clone with `path.format` and importFiles cache store - WORKS (wrong final css order)

